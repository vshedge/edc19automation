 /* Given I select any course from the Dashboard
 * 
 * When I have logged in with proper credentials
 * 
 * Then I should be able to view all the main headers of the course  
 * 
 * */ import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForAngularLoad(30)

WebUI.delay(2)

WebUI.setText(findTestObject('FROST/PagePreview/input_Admin_form-control search-frm ng-pristine ng-valid'), 'Sprint 4 Demo')

WebUI.click(findTestObject('FROST/PagePreview/button_Admin_btn btn-default btn-search'))

WebUI.waitForAngularLoad(30, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('FROST/TableOfContent/select_course'))

WebUI.waitForAngularLoad(30)

WebUI.verifyElementPresent(findTestObject('FROST/TableOfContent/div_entireScreen'), 30)

WebUI.verifyAllLinksOnCurrentPageAccessible(false, [])

