import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebElement element = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/FROST/LogoutAdmin/logout_dropdown'), 
    30)

WebUI.delay(1)

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('Object Repository/FROST/LoginAdmin/logout_dropdown'))

WebUI.executeJavaScript('arguments[0].click()', Arrays.asList(element))

//WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/btn_logout'))
WebElement element1 = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/FROST/LogoutAdmin/btn_logout'), 
    30)

WebUI.delay(1)

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('Object Repository/FROST/LogoutAdmin/btn_logout'))

WebUI.executeJavaScript('arguments[0].click()', Arrays.asList(element1))

WebUI.closeBrowser()

