 /*
 * Given I click on the Page Preview button after selecting any particular course
 * 
 * When I have logged in with proper credentials
 * 
 * Then I should be able to get a proper preview of the course available
 * 
 * */ import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/*WebUI.setText(findTestObject('FROST/PagePreview/input_Admin_form-control search-frm ng-pristine ng-valid'), 'Smoke Suit')

WebUI.click(findTestObject('FROST/PagePreview/button_Admin_btn btn-default btn-search'))*/
//WebUI.waitForPageLoad(30, FailureHandling.CONTINUE_ON_FAILURE)
WebUI.waitForAngularLoad(30)

WebUI.waitForElementVisible(findTestObject('FROST/PagePreview/input_Admin_form-control search-frm ng-pristine ng-valid'), 
    30)

WebUI.delay(5)

WebUI.setText(findTestObject('FROST/ExportProject/input_Admin_search'), 'Sprint 4 Demo')

WebUI.click(findTestObject('FROST/ExportProject/button_Admin_btn_search'))

WebUI.waitForAngularLoad(30)

WebUI.delay(2)

WebUI.click(findTestObject('FROST/ExportProject/div_Online Course'))

WebUI.waitForAngularLoad(30)

WebUI.click(findTestObject('FROST/PagePreview/button_Project Preview'))

WebUI.verifyElementPresent(findTestObject('FROST/PagePreview/table_content'), 20)

WebUI.verifyLinksAccessible([], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.takeScreenshot()

WebUI.click(findTestObject('FROST/PagePreview/button_Back to Dashboard'))

