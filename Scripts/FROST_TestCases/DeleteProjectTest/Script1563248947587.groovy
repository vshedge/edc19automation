import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.waitForAngularLoad(30)

WebUI.delay(5)

WebUI.setText(findTestObject('FROST/EditPageDetails/input_Admin_form-control search-frm ng-pristine ng-valid'), 'QA Test 123')

WebUI.click(findTestObject('FROST/EditPageDetails/button_Admin_btn btn-default btn-search'))

WebUI.waitForAngularLoad(30)

WebUI.delay(2)

WebUI.click(findTestObject('FROST/DeleteProject/button_Arindam_book-toc-icon'))

WebUI.click(findTestObject('FROST/DeleteProject/a_Delete Project'))

WebUI.click(findTestObject('FROST/DeleteProject/button_Yes delete it'))

//WebUI.click(findTestObject('FROST/DeleteProject/button_OK'))

WebElement element1 = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/FROST/DeleteProject/button_OK'),30)
WebUI.executeJavaScript("arguments[0].click()", Arrays.asList(element1))

