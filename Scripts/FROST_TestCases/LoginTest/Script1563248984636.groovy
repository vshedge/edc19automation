/*Given  the URL works
  
When I enter the proper login credentials

Then I should be able to login */ 


import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import com.ReuseableComponents
KeywordLogger log = new KeywordLogger()

WebUI.openBrowser('https://frostedc-stg.learningmate.com/#/')

WebUI.maximizeWindow()


CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/LoginPage/Page_ FROST/input_FROST Login_username'))
WebUI.setText(findTestObject('FROST/LoginPage/Page_ FROST/input_FROST Login_username'), 'arindam')

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/LoginPage/Page_ FROST/input_FROST Login_password'))
WebUI.setEncryptedText(findTestObject('FROST/LoginPage/Page_ FROST/input_FROST Login_password'), 'RigbBhfdqOBGNlJIWM1ClA==')


CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/LoginPage/Page_ FROST/button_Login'))
WebUI.click(findTestObject('FROST/LoginPage/Page_ FROST/button_Login'))

/*try {
    if (WebUI.verifyElementPresent(findTestObject('FROST/LoginPage/div_You already have an active sessionContinue'), 5, 
        FailureHandling.CONTINUE_ON_FAILURE)) {
        //WebUI.click(findTestObject('FROST/LoginPage/button_Continue'))
        WebElement element1 = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/FROST/LoginPage/button_Continue'), 
            30)

        WebUI.executeJavaScript('arguments[0].click()', Arrays.asList(element1))
    } else {
        WebUI.verifyElementPresent(findTestObject('FROST/LoginPage/btn_MyProjects'), 5, FailureHandling.CONTINUE_ON_FAILURE)
    }
}
catch (Exception e) {
    e.printStackTrace()
}*/


/*try {
	def value=WebUI.verifyElementVisible(findTestObject('FROST/LoginPage/div_You already have an active sessionContinue'), 5,
		FailureHandling.CONTINUE_ON_FAILURE)
	if (value==true) {
		//WebUI.click(findTestObject('FROST/LoginPage/button_Continue'))
		WebElement element1 = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/FROST/LoginPage/button_Continue'),
			30)

		WebUI.executeJavaScript('arguments[0].click()', Arrays.asList(element1))
	} else {
		WebUI.verifyElementVisible(findTestObject('FROST/LoginPage/btn_MyProjects'), 5, FailureHandling.CONTINUE_ON_FAILURE)
	}
}
catch (Exception e) {
	e.printStackTrace()
}
*/

boolean elementVisible=CustomKeywords.'com.ReuseableComponents.AlertMessageHandling.verifyObjectPresent'('Object Repository/FROST/LoginPage/div_You already have an active sessionContinue')
if (elementVisible==true){
	WebElement element1 = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/FROST/LoginPage/button_Continue'),
		30)

	CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('Object Repository/FROST/LoginPage/button_Continue'))
	WebUI.executeJavaScript('arguments[0].click()', Arrays.asList(element1))

	
} else {
	//log.logWarning('Customer has no Tasks available at this time')
	log.logInfo("Passed without clicking on CONTINUE button")
}
