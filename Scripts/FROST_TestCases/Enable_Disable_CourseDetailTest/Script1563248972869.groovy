 /*
 * Given I click on Enable/Disable button under Course Details
 * 
 * When I have logged in with proper credentials
 * 
 * Then I should be able to Enable/Disable it according to the requirement
 * 
 * */ import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/div_Welcome Arindam'))

WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/a_Go to Administration'))

WebUI.waitForAngularLoad(30)

WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/a_Courses'))

WebUI.setText(findTestObject('FROST/Enable_Disable_CourseDetail/text_searchBox'), 'edc')

WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/btn_search'))

WebUI.waitForAngularLoad(30)

WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/button_Course Details'))

/*for (int i = 0; i <= 1; i++) {
	
	value=WebUI.verifyElementPresent(findTestObject('FROST/Enable_Disable_CourseDetail/button_Enable'), 5, FailureHandling.CONTINUE_ON_FAILURE)
    
	if (value==true) {
        WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/button_Enable'))

        WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/button_OK'))
    } else {
        WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/button_Disable'))

        WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/button_OK'))
    }
}
*/
//CustomKeywords.'com.ReuseableComponents.forLooping'
WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/button_Enable'))

WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/button_OK'))

WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/button_Disable'))

WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/button_OK'), FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/dropdown_Logout'))

/*WebElement element = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/FROST/Enable_Disable_CourseDetail/dropdown_Logout'),30)
WebUI.executeJavaScript("arguments[0].click()", Arrays.asList(element))

//WebUI.click(findTestObject('FROST/Enable_Disable_CourseDetail/btn_logout'))
WebElement element1 = WebUiCommonHelper.findWebElement(findTestObject('FROST/Enable_Disable_CourseDetail/btn_logout'),30)
WebUI.executeJavaScript("arguments[0].click()", Arrays.asList(element1))
*/
