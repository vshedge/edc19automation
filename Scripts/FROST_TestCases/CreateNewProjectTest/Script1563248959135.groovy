/*
 * Given the dashboard loads properly
 *
 * When I enter the proper credentials
 *
 * Then I should be able to click on Create New Project button and create a new project 
 * 
 * */

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/NewProject/button_Create New Project'))
WebUI.click(findTestObject('FROST/CreateNewProject/NewProject/button_Create New Project'))

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/NewProject/input__objectName'))
WebUI.setText(findTestObject('FROST/CreateNewProject/NewProject/input__objectName'), 'QA Test 123')

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/NewProject/input_EDL Code_courseIdent'))
WebUI.setText(findTestObject('FROST/CreateNewProject/NewProject/input_EDL Code_courseIdent'), '12345')

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/Page_ FROST/textarea_Description_objectDescription'))
WebUI.setText(findTestObject('FROST/CreateNewProject/Page_ FROST/textarea_Description_objectDescription'), 'This is a sample project. ')

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/Page_ FROST/input_Version_course_version'))
WebUI.setText(findTestObject('FROST/CreateNewProject/Page_ FROST/input_Version_course_version'), '12345')

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/Page_ FROST/div_EDL003'))
WebUI.click(findTestObject('FROST/CreateNewProject/Page_ FROST/div_EDL003'))

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/NewProject/button_Create Project'))
WebUI.click(findTestObject('FROST/CreateNewProject/NewProject/button_Create Project'))

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/Page_ FROST/input_Admin_form-control search-frm ng-pristine ng-valid'))
WebUI.setText(findTestObject('FROST/CreateNewProject/Page_ FROST/input_Admin_form-control search-frm ng-pristine ng-valid'), 'QA Test 123')

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/Page_ FROST/button_Admin_btn btn-default btn-search'))
WebUI.click(findTestObject('FROST/CreateNewProject/Page_ FROST/button_Admin_btn btn-default btn-search'))

CustomKeywords.'com.ReuseableComponents.HighlightElement.highlight'(findTestObject('FROST/CreateNewProject/Page_ FROST/div_Online Course'))
WebUI.verifyElementPresent(findTestObject('FROST/CreateNewProject/Page_ FROST/div_Online Course'), 30)


