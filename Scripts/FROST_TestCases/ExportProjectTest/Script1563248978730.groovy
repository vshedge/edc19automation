import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.waitForAngularLoad(30)

WebUI.delay(5)

WebUI.setText(findTestObject('FROST/ExportProject/input_Admin_search'), 'Sprint 4 Demo')

WebUI.click(findTestObject('FROST/ExportProject/button_Admin_btn_search'))

WebUI.waitForAngularLoad(30)

WebUI.delay(2)

WebUI.click(findTestObject('FROST/ExportProject/div_Online Course'))

WebUI.click(findTestObject('FROST/ExportProject/img_Success_ng-scope'))

WebUI.click(findTestObject('FROST/ExportProject/button_Export Course'))

WebUI.selectOptionByValue(findTestObject('FROST/ExportProject/select_HTML'), 'imscc12lti', true)

WebUI.click(findTestObject('FROST/ExportProject/input_Prefered Shell_publish_all'))

WebUI.click(findTestObject('FROST/ExportProject/button_Yes export it'))

WebUI.click(findTestObject('FROST/ExportProject/button_OK'))

WebUI.click(findTestObject('FROST/ExportProject/div_Welcome Arindam'))

WebUI.click(findTestObject('FROST/ExportProject/a_Go to Administration'))

WebUI.click(findTestObject('FROST/ExportProject/a_Courses'))

WebUI.setText(findTestObject('FROST/ExportProject/input_Course Name_inputEmail3'), 'QA1234')

WebUI.click(findTestObject('FROST/ExportProject/button_Search'))

WebElement element1 = WebUiCommonHelper.findWebElement(findTestObject('FROST/ExportProject/button_Course Details'),30)
WebUI.executeJavaScript("arguments[0].click()", Arrays.asList(element1))

//WebUI.click(findTestObject('FROST/ExportProject/button_Course Details'))

WebUI.click(findTestObject('FROST/ExportProject/button_Download'))

