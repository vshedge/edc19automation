 /*
 * Given when I click on the Edit Project button
 * 
 * When I have a logged in with proper credentials
 * 
 * Then I should be able to make changes in the project details
 * 
 * */ import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForAngularLoad(30)

WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('FROST/EditPageDetails/input_Admin_form-control search-frm ng-pristine ng-valid'), 
    30)

WebUI.setText(findTestObject('FROST/EditPageDetails/input_Admin_form-control search-frm ng-pristine ng-valid'), 'Smoke Suite')

WebUI.click(findTestObject('FROST/EditPageDetails/button_Admin_btn btn-default btn-search'))

WebUI.waitForAngularLoad(30)

WebUI.delay(5)

WebUI.click(findTestObject('FROST/EditPageDetails/div_Online Course (1)'))

WebUI.click(findTestObject('FROST/EditPageDetails/div_Edit Project'))

WebUI.clearText(findTestObject('FROST/EditPageDetails/input__objectName'))

WebUI.setText(findTestObject('FROST/EditPageDetails/input__objectName'), 'Smoke Suite 2.0')

WebUI.clearText(findTestObject('FROST/EditPageDetails/input_EDL Code_courseIdent'))

WebUI.setText(findTestObject('FROST/EditPageDetails/input_EDL Code_courseIdent'), '3210')

WebUI.click(findTestObject('FROST/EditPageDetails/div_EDL002'))

WebUI.click(findTestObject('FROST/EditPageDetails/button_Save Changes'))

WebUI.verifyElementPresent(findTestObject('FROST/EditPageDetails/div_                Success Project updated successfully'), 
    0)

/*WebUI.click(findTestObject('FROST/EditPageDetails/a_Frost'))

WebUI.setText(findTestObject('FROST/EditPageDetails/input_Admin_form-control search-frm ng-pristine ng-valid'), 'Smoke Suite 2.0')

WebUI.click(findTestObject('FROST/EditPageDetails/button_Admin_btn btn-default btn-search'))

WebUI.waitForAngularLoad(30)

WebUI.delay(5)

WebUI.click(findTestObject('FROST/EditPageDetails/div_Online Course (1)'))

def result = WebUI.getText(findTestObject('FROST/EditPageDetails/div_QA1234 Smoke Suit'))

WebUI.verifyMatch(result, 'Smoke Suite 2.0', true)

*/