import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('CANVAS/Logout/logoutLink'))



WebElement element1 = WebUiCommonHelper.findWebElement(findTestObject('CANVAS/Logout/logoutButton'),30)
WebUI.executeJavaScript("arguments[0].click()", Arrays.asList(element1))
//WebUI.click(findTestObject('CANVAS/Logout/logoutButton'))

WebUI.closeBrowser()

