<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Enable_Disable_CourseDetails_TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d099c7a0-72c4-4832-a848-093223987018</testSuiteGuid>
   <testCaseLink>
      <guid>ccb4ad46-c836-45c6-97cf-e3bc6f46779a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LoginTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33ac2864-4e56-48ed-b55e-aa68f421a6d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/Enable_Disable_CourseDetailTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c290d98f-7eea-4c68-aa03-07202e23734f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LogoutAdminTest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
