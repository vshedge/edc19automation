<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>DeleteProject_TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>277271d5-0cdc-471f-8481-2e4b6e5d8709</testSuiteGuid>
   <testCaseLink>
      <guid>3481bf57-94f0-4e7b-a432-5723e9b05856</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LoginTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56353841-d1a8-4729-9b66-1eda509b5cdf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/DeleteProjectTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc6b1a2a-6089-45b1-bd65-3a2dd9d16977</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LogoutDashboardTest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
