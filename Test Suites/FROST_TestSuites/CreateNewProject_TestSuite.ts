<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>CreateNewProject_TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b6c261e0-0cd0-4df4-b608-4559bfbd6454</testSuiteGuid>
   <testCaseLink>
      <guid>2caff93c-2a36-4ca1-984e-566a754e67d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LoginTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87525e3b-235a-4b97-adbb-a6010c621756</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/CreateNewProjectTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c9e01d9-c045-44de-8871-742c2edeeb9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LogoutDashboardTest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
