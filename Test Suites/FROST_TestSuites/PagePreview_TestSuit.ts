<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>PagePreview_TestSuit</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9e408ad5-0e25-4dd8-a293-5cc19c7645ce</testSuiteGuid>
   <testCaseLink>
      <guid>944fdd81-f2c0-4ebd-96f0-077b73f06906</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LoginTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c80f03ff-1eec-4c25-b1cb-59ed4b58723c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/PagePreviewTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccdc4743-1df9-49d7-8487-8a98d5e6933e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LogoutDashboardTest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
