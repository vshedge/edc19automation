<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>EditProjectDetails_TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1b9336fc-510a-4330-9a9e-5fee51c0cff2</testSuiteGuid>
   <testCaseLink>
      <guid>c1fac0cd-5416-4aeb-9312-9be44065fb54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LoginTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66074453-0891-4ffe-8e47-ea30216bdbf3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/EditProjectDetailsTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f65b0ed-286c-46d6-9387-4225ff9ecda7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LogoutDashboardTest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
