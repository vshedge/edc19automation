<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TableOfContent_TestSuit</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a0064abf-1e5f-4ed2-bc74-122833ec87e7</testSuiteGuid>
   <testCaseLink>
      <guid>477b9a36-cfcd-4f9c-9c1a-faba9c8a8c34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LoginTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fbc2af3-e819-4d99-8148-7bea07fa0fab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/TableOfContentTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c7d1573-19d5-4f52-880a-2b58b94610d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FROST_TestCases/LogoutDashboardTest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
