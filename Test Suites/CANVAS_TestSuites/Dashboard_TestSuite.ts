<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Dashboard_TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d24d4079-2019-4f99-b153-d34a2566985b</testSuiteGuid>
   <testCaseLink>
      <guid>af50a530-2b58-4827-9644-8f212890cf31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CANVAS_TestCases/LoginTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76ed858a-0b9c-4120-b81f-a3d698c7cc85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CANVAS_TestCases/DashboardTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57973415-a130-4426-949b-6fd9ec9d3917</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CANVAS_TestCases/LogoutTest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
