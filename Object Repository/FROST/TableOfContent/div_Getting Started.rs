<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Getting Started</name>
   <tag></tag>
   <elementGuidId>3d2c8b4f-47a4-48dc-8286-9d28c0912504</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Unit'])[1]/following::div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>lirow</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-mouseenter</name>
      <type>Main</type>
      <value>tocRowHoverClass = 'tocRowHover'</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-mouseleave</name>
      <type>Main</type>
      <value>tocRowHoverClass = ''</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                        
                        
                            
                            
                                
                            
                        
                        

                        
                            Getting Started 
                                
                          
                        
                        
                        
                        
                        
                        
                        
                        
                         
                        
                        
                            - 
                        
                        
                        
                        
                        

                        

                        

                        

                        

                        

                        
                                                                                   

                        
                            
                                
                            
                        
                                                
                         
                  

                        
                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;ajax-content ng-scope&quot;]/div[@class=&quot;main_container ng-scope&quot;]/section[@class=&quot;wrapper&quot;]/div[@class=&quot;container product-inner&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;from-container&quot;]/div[@class=&quot;from-container-row&quot;]/div[@class=&quot;toc-panel-container&quot;]/div[@class=&quot;live ng-scope&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;toc-panel-left tc-cont ng-scope&quot;]/div[@class=&quot;toccontent-container&quot;]/ol[@class=&quot;sortable ui-sortable&quot;]/li[@class=&quot;ng-scope&quot;]/div[@class=&quot;toc-outer tocRowHover&quot;]/div[@class=&quot;lirow&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Unit'])[1]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Issues'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
