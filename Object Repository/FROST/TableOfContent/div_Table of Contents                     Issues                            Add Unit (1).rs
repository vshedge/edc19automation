<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Table of Contents                     Issues                            Add Unit (1)</name>
   <tag></tag>
   <elementGuidId>5d610d87-570a-4799-88c3-a38add0eaba6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Grade Category'])[1]/following::div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page-title</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Table of Contents 
        
            Issues
        
        
            Add Unit
        
          
    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;ajax-content ng-scope&quot;]/div[@class=&quot;main_container ng-scope&quot;]/section[@class=&quot;wrapper&quot;]/div[@class=&quot;container product-inner&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;from-container&quot;]/div[@class=&quot;from-container-row&quot;]/div[@class=&quot;toc-panel-container&quot;]/div[@class=&quot;live ng-scope&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;toc-panel-left tc-cont ng-scope&quot;]/div[@class=&quot;page-title&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Grade Category'])[1]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Utilities'])[1]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
