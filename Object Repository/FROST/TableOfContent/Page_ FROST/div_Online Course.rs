<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Online Course</name>
   <tag></tag>
   <elementGuidId>95f73cb7-e7ac-4237-a7c2-afad4dda6658</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>

body > div.ng-scope > div > div.main_container.ng-scope > div.content-wrap > section > div.container.product-inner > div.content > div.from-container.grid-view.ng-scope > div > div.wrapper-row.dashboard-container > div:nth-child(1) > div.theme-box-col > div > div.box-img-out > div > div > img</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Success!'])[1]/following::div[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>theme-box-col</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            
                                                
                                                    
                                                        
                                                            
                                                            
                                                            Online Course
                                                        
                                                    
                                                
                                                QA1234 Smoke Suit

                                                
                                                    Arindam
                                                    
                                                        
                                                    
                                                         
                                                
                                                
                                                    0
                                                    

                                                    
                                                        
                                                            
                                                        
                                                    
                                                    
                                                

                                                
                                                    
                                                         Edit Project
                                                        
                                                        
                                                        View TOC
                                                        
                                                              Define Pattern
                                                        
                                                         Define Asset
                                                         Define Glossary
                                                        
                                                        Duplicate Project
                                                        
                                                             Delete Project
                                                            
                                                        
                                                    
                                                    
                                                



                                            
                                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;ajax-content ng-scope&quot;]/div[@class=&quot;main_container ng-scope&quot;]/div[@class=&quot;content-wrap&quot;]/section[@class=&quot;wrapper&quot;]/div[@class=&quot;container product-inner&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;from-container grid-view ng-scope&quot;]/div[@class=&quot;from-container-row&quot;]/div[@class=&quot;wrapper-row dashboard-container&quot;]/div[@class=&quot;col-md-3 col-sm-4 prod-block ng-scope&quot;]/div[@class=&quot;theme-box-col&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Success!'])[1]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
