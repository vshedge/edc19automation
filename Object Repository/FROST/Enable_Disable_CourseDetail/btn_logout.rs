<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_logout</name>
   <tag></tag>
   <elementGuidId>e23bfad2-8af6-4f2b-af33-e1f8c961578c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#wrapper > div.ng-isolate-scope > div > div.pull-right.admin-topheader > ul > li.dropdown.navbar-profile.userpadding.open > ul > li:nth-child(4) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
