<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_Logout</name>
   <tag></tag>
   <elementGuidId>87b07ca4-fd79-42de-893a-dd9757339980</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#wrapper > div.ng-isolate-scope > div > div.pull-right.admin-topheader > ul > li:nth-child(2) > a</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@class='dropdown-toggle']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
