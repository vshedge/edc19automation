<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Online Course</name>
   <tag></tag>
   <elementGuidId>44f930de-8416-4221-a022-5c81a691805d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>.//div[text()='Sprint 4 Demo 02-05-19']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                            
                                                            
                                                            Online Course
                                                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;ajax-content ng-scope&quot;]/div[@class=&quot;main_container ng-scope&quot;]/div[@class=&quot;content-wrap&quot;]/section[@class=&quot;wrapper&quot;]/div[@class=&quot;container product-inner&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;from-container grid-view ng-scope&quot;]/div[@class=&quot;from-container-row&quot;]/div[@class=&quot;wrapper-row dashboard-container&quot;]/div[@class=&quot;col-md-3 col-sm-4 prod-block ng-scope&quot;]/div[@class=&quot;theme-box-col&quot;]/div[@class=&quot;box-theme&quot;]/div[@class=&quot;box-img-out&quot;]/div[@class=&quot;block-img&quot;]/div[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Success!'])[1]/following::div[11]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='QA1234 Smoke Suit'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
