<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_logout</name>
   <tag></tag>
   <elementGuidId>4e2ffc07-5903-4463-8f15-2e3c20eed610</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#wrapper > div.ng-isolate-scope > div > div.pull-right.admin-topheader > ul > li.dropdown.navbar-profile.userpadding.open > ul > li:nth-child(4) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
