<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>logout_dropdown</name>
   <tag></tag>
   <elementGuidId>7431ba34-f14c-4bac-88cd-8e9f7e51e3c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#wrapper > div.ng-isolate-scope > div > div.pull-right.admin-topheader > ul > li:nth-child(2) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
