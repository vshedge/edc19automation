<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Online Course</name>
   <tag></tag>
   <elementGuidId>15eb4ad0-71f7-49f3-99b9-91e49c5a2d71</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Success!'])[1]/following::div[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-3 col-sm-4 prod-block ng-scope</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>total-items</name>
      <type>Main</type>
      <value>totalProjects</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>current-page</name>
      <type>Main</type>
      <value>currentPage</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>pagination-id</name>
      <type>Main</type>
      <value>projectList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-init</name>
      <type>Main</type>
      <value>toggler = 0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-repeat</name>
      <type>Main</type>
      <value>project in projectlist.data | itemsPerPage: itemsPerPage : 'projectList'</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            
                                                
                                                    
                                                        
                                                            
                                                            
                                                            Online Course
                                                        
                                                    
                                                
                                                QA1234 Smoke Suit

                                                
                                                    Arindam
                                                    
                                                        
                                                    
                                                         
                                                
                                                
                                                    0
                                                    

                                                    
                                                        
                                                            
                                                        
                                                    
                                                    
                                                

                                                
                                                    
                                                         Edit Project
                                                        
                                                        
                                                        View TOC
                                                        
                                                              Define Pattern
                                                        
                                                         Define Asset
                                                         Define Glossary
                                                        
                                                        Duplicate Project
                                                        
                                                            
                                                             Delete Project
                                                        
                                                    
                                                    
                                                



                                            
                                        
                                        
                                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;ajax-content ng-scope&quot;]/div[@class=&quot;main_container ng-scope&quot;]/div[@class=&quot;content-wrap&quot;]/section[@class=&quot;wrapper&quot;]/div[@class=&quot;container product-inner&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;from-container grid-view ng-scope&quot;]/div[@class=&quot;from-container-row&quot;]/div[@class=&quot;wrapper-row dashboard-container&quot;]/div[@class=&quot;col-md-3 col-sm-4 prod-block ng-scope&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Success!'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
