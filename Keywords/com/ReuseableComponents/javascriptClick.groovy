package com.ReuseableComponents

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class javascriptClick {

	@Keyword

	public static void clickOnElement(TestObject to){

		WebElement element = WebUiCommonHelper.findWebElement(to, 20);
		WebUI.executeJavaScript("arguments[0].click()", Arrays.asList(element))
	}


	@Keyword
	public boolean verifyObjectPresent(String objectReference) {
		try {
			WebUiCommonHelper.findWebElement(findTestObject(objectReference),5)
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
