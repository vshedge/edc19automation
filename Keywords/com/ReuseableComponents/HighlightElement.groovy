package com.ReuseableComponents

import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class HighlightElement {

	static WebDriver driver = DriverFactory.getWebDriver()

	@Keyword

	public void highlight(TestObject object) {

		JavascriptExecutor js = (JavascriptExecutor) driver;

		//WebElement element = WebUI.findWebElement(object);
		WebElement element = WebUiCommonHelper.findWebElement(object, 20);
		//js.executeScript("arguments[0].setAttribute('style', 'background: white; border: 2px solid red;');", element);
		//	List<WebElement> elements = WebUiBuiltInKeywords.findWebElements(to, 1)
		// WebUI.executeJavaScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", Arrays.asList(element))

		js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,"border: 3px solid red;");
		js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,"border:' '; ");

		println("This is a java code..Welcome to Java!")
	}
}


