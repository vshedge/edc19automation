package com.ReuseableComponents

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory

public class AlertMessageHandling {

	@Keyword
	public boolean verifyObjectPresent(String objectReference) {
		try {
			
			WebUiCommonHelper.findWebElement(findTestObject(objectReference),5)
			return true;
			
		} catch (Exception e) {
			return false;
		}
	}



	/*@Keyword
	 public void takeElement(TestObject object){
	 WebElement element=WebUiCommonHelper.findWebElement(object, 20)
	 WebDriver driver=DriverFactory.getWebDriver()
	 Screenshot screenshot=new AShot().takeScreenshot(driver,element)
	 }*/
}
