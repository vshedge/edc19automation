import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/FROST_TestSuites/ExportProject_TestSuite')

suiteProperties.put('name', 'ExportProject_TestSuite')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\mahesh.nair\\Katalon Studio\\Frost\\Reports\\FROST_TestSuites\\ExportProject_TestSuite\\20190717_143423\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/FROST_TestSuites/ExportProject_TestSuite', suiteProperties, [new TestCaseBinding('Test Cases/FROST_TestCases/LoginTest', 'Test Cases/FROST_TestCases/LoginTest',  null), new TestCaseBinding('Test Cases/FROST_TestCases/ExportProjectTest', 'Test Cases/FROST_TestCases/ExportProjectTest',  null), new TestCaseBinding('Test Cases/FROST_TestCases/LogoutAdminTest', 'Test Cases/FROST_TestCases/LogoutAdminTest',  null)])
