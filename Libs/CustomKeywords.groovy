
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import com.kms.katalon.core.testobject.TestObject

import java.lang.String


def static "com.ReuseableComponents.javascriptClick.clickOnElement"(
    	TestObject to	) {
    (new com.ReuseableComponents.javascriptClick()).clickOnElement(
        	to)
}

def static "com.ReuseableComponents.javascriptClick.verifyObjectPresent"(
    	String objectReference	) {
    (new com.ReuseableComponents.javascriptClick()).verifyObjectPresent(
        	objectReference)
}

def static "com.ReuseableComponents.AlertMessageHandling.verifyObjectPresent"(
    	String objectReference	) {
    (new com.ReuseableComponents.AlertMessageHandling()).verifyObjectPresent(
        	objectReference)
}

def static "com.ReuseableComponents.HighlightElement.highlight"(
    	TestObject object	) {
    (new com.ReuseableComponents.HighlightElement()).highlight(
        	object)
}
